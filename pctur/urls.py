from django.conf.urls import url

from Main import  views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^placeholder/(?P<size>\w+)/$', views.placeholder, name="placeholder"),
    url(r'^thumb/(?P<size>\d+)/$', views.thumb),
    url(r'^crop/(?P<size>\w+)/$', views.crop),
    url(r'^resize/(?P<size>\w+)/$', views.resize),
    url(r'^resize/w/(?P<width>\d+)/$', views.resizebywidth),
    url(r'^resize/h/(?P<height>\d+)/$', views.resizebyheight),
    url(r'^resizethencrop/(?P<size>\w+)/$', views.resizethencrop),
    url(r'^resizeorcrop/(?P<size>\w+)/$', views.resizeorcrop),
    url(r'^rotate/(?P<degrees>\d+)/$', views.rotate),
    url(r'^blur/$', views.blur),
    url(r'^add-text/(?P<text>\w+)/(?P<hex>\w+)/$', views.add_text),
    url(r'^watermark/$', views.add_watermark),
    url(r'^round/(?P<radius>\d+)/$', views.round),
]
