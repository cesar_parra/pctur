from PIL import Image, ImageFilter, ImageFont, ImageDraw, ImageOps
import os


class ImageProcessor:
    def create_placeholder(self, size):
        '''
        Creates a gray placeholder image
        :param size: Tuple (width, height)
        :return:
        '''
        placeholder_image = Image.new('RGBA', size, "#ddd")
        size_str = str(size[0]) + "x" + str(size[1])
        #placeholder_image = self.add_text(placeholder_image, size_str)
        return placeholder_image

    def create_thumbnail(self, image, size):
        '''
        Creates a thumbnail of the passed in image
        :param image:
        :param size: Tuple (width, height)
        :return:
        '''
        image.thumbnail(size, Image.ANTIALIAS)
        return image

    def crop(self, image, desired_width, desired_height):
        '''
        Crops an image to the desired size from the center
        :param image:
        :param desired_width:
        :param desired_height:
        :return:
        '''
        width, height = image.size
        left = (width / 2) - (desired_width / 2)
        top = (height / 2) - (desired_height / 2)
        right = (width / 2) + (desired_width / 2)
        bottom = (height / 2) + (desired_height / 2)
        box_tuple = (left, top, right, bottom)
        cropped = image.crop(box_tuple)
        return cropped

    def resize(self, image, size):
        '''
        Resized an image to the desired size
        :param image: Tuple (width, height)
        :param size:
        :return:
        '''
        resized = image.resize(size)
        return resized

    def resize_by_height(self, image, height):
        '''
        Resizes the height of an image, keeping its proportions
        :param image:
        :param height:
        :return:
        '''
        hpercent = (height/float(image.size[1]))
        wsize = int((float(image.size[0])*float(hpercent)))
        resized_image = image.resize((wsize,height), Image.ANTIALIAS)
        return resized_image

    def resize_by_width(self, image, width):
        '''
        Resizes the width of an image, keeping its proportions
        :param image:
        :param width:
        :return:
        '''
        wpercent = (width/float(image.size[0]))
        hsize = int((float(image.size[1])*float(wpercent)))
        resized_image = image.resize((width,hsize), Image.ANTIALIAS)
        return resized_image

    def resize_then_crop(self, image, size):
        '''
        If the image is larger than the desired size, then resize to fit the larger part of the image
        then, if the image is still larger, then crop.
        If the image is smaller, it will first be resized keeping proportions
        and then it will be cropped to the desired size
        :param image:
        :param size:
        :return:
        '''
        if self._is_image_larger(image, size):
            if size[0] > size[1]:
                # If we want an image that is wider than taller, let's resize by width
                image = self.resize_by_width(image, size[0])
            else:
                # If we want a wider image, let's resize by height
                image = self.resize_by_height(image, size[1])

        image_size = image.size
        if image_size[0] < size[0]:
            # If the width of the image is smaller than the desired width, let's match that width
            image = self.resize_by_width(image, size[0])
            image_size = image.size

        if image_size[1] < size[1]:
            # If the height of the image is smaller than the desired height, let's match that height
            image = self.resize_by_height(image, size[1])

        if image.size == size:
            # If we have our desired size, let's return
            return image

        # If we don't have our desired size, let's crop it
        return self.crop(image, size[0], size[1])

    def resize_or_crop(self, image, size):
        '''
        If width and height of the image are > desired size, crop, if not then resize
        :param image:
        :param size:
        :return:
        '''
        if self._is_image_larger(image, size):
            return self.crop(image, size[0], size[1])

        return self.resize(image, size)

    def _is_image_larger(self, image, size):
        '''
        Checks if an image is larger than the size passed in. An image is larger if both the width and
        the height are larger than the width and size passed in
        :param image: Image to check againts
        :param size: Tuple (width, height)
        :return:
        '''
        image_size = image.size
        return image_size > size

    def rotate(self, image, degrees):
        rotated = image.rotate(degrees)
        return rotated

    def blur(self, image):
        blurred = image.filter(ImageFilter.BLUR)
        return blurred

    def add_text(self, image, text_to_add, color="aaa"):
        image = image.convert('RGBA')
        txt = Image.new('RGBA', image.size, (50, 50, 50, 0))

        SITE_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        path_to_static = os.path.join(SITE_ROOT, 'static/OpenSans-Regular.ttf')
        font = ImageFont.truetype(path_to_static, size=40)

        d = ImageDraw.Draw(txt)
        text_width, text_height = font.getsize(text_to_add)
        d.text(((image.size[0] - text_width) / 2, (image.size[1] - text_height) / 2), text_to_add, font=font, fill="#"+color)
        image_with_text = Image.alpha_composite(image, txt)

        return image_with_text


    def add_watermark(self, image):
        image = image.convert('RGBA')
        image_with_lines = Image.new('RGBA', image.size, (255, 255, 255, 0))
        draw = ImageDraw.Draw(image_with_lines)
        draw.line((0, 0) + image_with_lines.size, fill=(255,255,255,128), width=3)
        draw.line((0, image_with_lines.size[1], image_with_lines.size[0], 0), fill=(255,255,255,128), width=3)
        del draw

        image_with_lines = Image.alpha_composite(image, image_with_lines)
        #image_with_lines = self.add_text(image_with_lines, 'watermark')

        return image_with_lines

    def get_circular(self, image, radius):
        image = image.convert('RGBA')
        size = (radius, radius)
        mask = Image.new('L', size, 0)
        draw = ImageDraw.Draw(mask)
        draw.ellipse((0, 0) + size, fill=255)
        cropped_image = ImageOps.crop(image, radius)
        #If I just pass the original image instead of the cropped image, it will be resized and turned into a circle
        output = ImageOps.fit(image, mask.size, centering=(0.5, 0.5))
        output.putalpha(mask)

        return output
