from django.http import HttpResponse
from django.shortcuts import render

from Main.Classes.ImageProcessor import ImageProcessor
from PIL import Image
import io
import urllib.request

def index(request):
    return render(request, 'Main/index.html', {
        'ImagePath' : 'http://3.bp.blogspot.com/-MPonaRGPb1g/TwRiaOo-BKI/AAAAAAAAAFI/Lmv_8tH1jK8/s1600/chicago-skyscrapers-1440x900.jpg',
    })

def placeholder(request, size):
    '''
    Returns a placeholder image
    :param request:
    :param size:
    :return:
    '''
    size_list = size.split('x')
    size = (int(size_list[0]), int(size_list[1]))

    placeholder_image = ImageProcessor().create_placeholder(size)
    return _get_response(placeholder_image)

def thumb(request, size):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    image_size = (int(size), int(size))
    image_thumb = ImageProcessor().create_thumbnail(image, image_size)
    return _get_response(image_thumb)

def crop(request, size):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    size_list = size.split('x')
    size = (int(size_list[0]), int(size_list[1]))

    cropped_image = ImageProcessor().crop(image, size[0], size[1])
    return _get_response(cropped_image)


def resize(request, size):
    url = _get_img_path(request)
    image = _get_image_from_url(url)
    size_list = size.split('x')
    size = (int(size_list[0]), int(size_list[1]))

    resized_image = ImageProcessor().resize(image, size)
    return _get_response(resized_image)

def resizebywidth(request, width):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    resized_image = ImageProcessor().resize_by_width(image, int(width))
    return _get_response(resized_image)

def resizebyheight(request, height):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    resized_image = ImageProcessor().resize_by_height(image, int(height))
    return _get_response(resized_image)

def resizethencrop(request, size):
    url = _get_img_path(request)
    image = _get_image_from_url(url)
    size_list = size.split('x')
    size = (int(size_list[0]), int(size_list[1]))

    resized_image = ImageProcessor().resize_then_crop(image, size)
    return _get_response(resized_image)

def resizeorcrop(request, size):
    url = _get_img_path(request)
    image = _get_image_from_url(url)
    size_list = size.split('x')
    size = (int(size_list[0]), int(size_list[1]))

    resized_image = ImageProcessor().resize_or_crop(image, size)
    return _get_response(resized_image)

def rotate(request, degrees):
    url = _get_img_path(request)
    image = _get_image_from_url(url)
    rotation = int(degrees)

    rotated_image = ImageProcessor().rotate(image, rotation)
    return _get_response(rotated_image)

def blur(request):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    blurred_image = ImageProcessor().blur(image)
    return _get_response(blurred_image)


def add_text(request, text, hex):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    image_with_text = ImageProcessor().add_text(image, text, hex)
    return _get_response(image_with_text)


def add_watermark(request):
    url = _get_img_path(request)
    image = _get_image_from_url(url)

    watermarked = ImageProcessor().add_watermark(image)
    return _get_response(watermarked)


def round(request, radius):
    url = _get_img_path(request)
    image = _get_image_from_url(url)
    radius = int(radius)

    rounded_image = ImageProcessor().get_circular(image, radius)
    return _get_response(rounded_image)


def _get_image_from_url(url):
    opened_url = urllib.request.urlopen(url)
    file = io.BytesIO(opened_url.read())
    image = Image.open(file)

    return image

def _get_response(image):
    response = HttpResponse(content_type="image/png")
    image.save(response, "PNG")
    return response

def _get_img_path(request):
    return request.GET.get('img_path')